# README #

* Currently this has been tested on RHEL, but should also work on CentOS. Debian and Ubuntu would require running dpkg-query instead of rpm -q
* PE files were pulled from 3.8 and have not been tested on newer versions

### What is this repository for? ###

* This repository removes puppet and pe-agent from servers
* Version: Production


### Deployment instructions ###

* Update the inventory file
* Run the playbook below 

##### Run the playbook #####
  * ansible-playbook --extra-vars "ansible_ssh_user=root" --ask-pass -i inventory site.yml
  
### Who do I talk to? ###

* Joe Bliss